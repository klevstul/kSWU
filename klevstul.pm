# ---------------------------------------------
# filename:	klevstul.pm
# author:	Frode Klevstul (frode@klevstul.com)
# started:	16.12.2004
# version:	v01_20041216
# ---------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v01_20050813
#			- Fixed an error in kMakeLinks (didn't parse '~' as a link)
# version:  v01_20041216
#           - The first version of the 'klevstul library' was made.
# ------------------------------------------------------------------------------------



# --------------------------------------------------
# package setup
# --------------------------------------------------
package klevstul;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	kGetIniValue
	kPrintHtmlPage
	kStartPage
	kQuery
	kGetPath
	kErrorHTML
	kError
	kPrint
	kJumpTo
	kGetDir
	kGetFilename
	kListDirectory
	kHTMLify
	kArrayOp
	kMakeLinks
	kStripLinks
);


# -----------------------------------------------
# use
# -----------------------------------------------
use CGI;
use LWP::Simple;


# -----------------------------------------------
# Global declarations
# -----------------------------------------------


# -----------------------------------------------
# sub rutines
# -----------------------------------------------



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		Opens a file and returns a parameter value.
#			The file has to be on the format:
#			PARAMETER_NAME		PARAMETER_VALUE
#			(name and value has to be separated by tab)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kGetIniValue{
	my $ini_file = $_[0];						# the name of the file that is going to be opened
	my $parameter = $_[1];						# the name of the parameter to what we want it's value
	my $ignore_value = $_[2];					# if this variable is equal to 1, then we ignore if no value is found
	my $stop_value = $_[3];						# this variable tells us if we just want first entry in a file or all values
												# if no value (undef), returns all entries. if value == 1, return first entrie
	my $line;
	my @value;									# NB: This procedure returns an array!

    open(FILE_INI, "<$ini_file") || &kError("klevstul::kGetIniValue","failed to open '$ini_file'");
	LINE: while ($line = <FILE_INI>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			push(@value,$2);
			if ($stop_value == 1){
				last LINE;
			}
		}	
	}
	close (FILE_INI);

	if ($value[0] eq "" and $ignore_value < 1){
		&kError("klevstul::kGetIniValue", "failed to find a value for parameter '$parameter' in '$ini_file'", 1);
	}
	
	if ($value[0] eq "NULL"){
		$value[0] = "";
	}
	
	return @value;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		print out an HTML page
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kPrintHtmlPage{
	my $url = $_[0];
	my $object;
	my @lines;

	if ($url ne ""){
		$object = &get($url);
	}
	
	@lines = split ("\n", $object);
	
	foreach (@lines){
		$_ =~ s/^\s{2,}//;																			# trim heading spaces
		$_ =~ s/\s{2,}$//;																			# trim trailing spaces
		print $_ . "\n";
	}

}




# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		print out the start text for an page to 
#			be treated as a HTML page
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kStartPage{
	my $type = $_[0];
	
	if ($type eq "HTML"){
		print "Content-type: text/html\n\n";
	}
}




# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		returns a value from the CGI library
#			function
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kQuery{
	$parameter	= $_[0];
	my $query	= new CGI;
	
	return $query->param($parameter);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		find and return the path to where the 
#			program is running
# - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kGetPath{
	$0=~/^(.+[\\\/])[^\\\/]+[\\\/]*$/;
	my $cgidir= $1 || "./";
	return $cgidir;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		prints out HTML error message
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kErrorHTML{
	my $module			= $_[0];
	my $error_msg		= $_[1];
	my $exit			= $_[2];
	
	&kPrint("<font color=\"#FF0000\">Error in module '$module': $error_msg</font><br><br>");

	if ($exit){
		exit;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		prints out an error message (not HTML)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kError{
	my $module		= $_[0];
	my $error_msg	= $_[1];
	my $exit		= $_[2];

	&kPrint("Error in module '$module': $error_msg\n");
	
	if ($exit){
		exit;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		prints out message on screen
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kPrint{
	my $text = $_[0];
	
	print $text . "\n";
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		jump to another URL
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kJumpTo{
	my $url			= $_[0];

	print qq(<html><head><title></title>
		<META HTTP-EQUIV="REFRESH" Content="0;URL=$url">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		</head><body>Please wait...</body></html>
	);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		return the directory part of a path to a 
#			file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kGetDir{
	my $full_path = $_[0];
	
	if($full_path =~ m/[\/\\]/){
		$full_path =~ s/(.*)[\/\\].*/$1/;
	} else {
		$full_path = "";
	}
	return $full_path;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		return the filename part of a path to a 
#			file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kGetFilename{
	my $full_path = $_[0];
	$full_path =~ s/.*[\/\\](.*)/$1/;
	return $full_path;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		return the filenames of all files in given
#			directory
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kListDirectory{
	$dir		= $_[0];
	$files_only	= $_[1];

	my @files;
	my @files_only;


	opendir (DIR, "$dir");
	@files = grep /\w/, readdir(DIR);
	closedir(DIR);

	if (!$files_only){
		return @files;
	} else {
		foreach (sort @files){	
			if (!-d $dir . $_){
				push (@files_only, $_);
			}
		}
		return @files_only;
	}
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		replace special character with it's HTML
#			representation
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kHTMLify{
	$text = $_[0];
	
	$text =~ s/</&lt;/g;
	$text =~ s/>/&gt;/g;
	$text =~ s/\|/&brvbar;/g;
	$text =~ s/\\/&#92;/g;
	$text =~ s/\\/&#92;/g;
	$text =~ s/"/&quot;/g;

	return $text;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		performs different operations on a array
#			and returns a new modifed array
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kArrayOp{
	my ($operation, $position, @in_array) = @_;
	my @out_array = @in_array;
	my $skip_next;
	my $i;
	my $o;
	
	FOREACH: foreach (@in_array){
		if  ($operation eq "up" && $i == $position-1){
			$out_array[$i]		= $in_array[$position];												# this element will be replaced/swapped with the next element
			$out_array[$i+1]	= $in_array[$i];													# the next element will be replaced with this element
			last FOREACH;
		} elsif ($operation eq "down" && $i == $position) {
			$out_array[$i+1]	= $in_array[$position];												# next element is swapped with this element
			$out_array[$i]		= $in_array[$i+1];													# this element is swapped with next element
			last FOREACH;			
		} elsif ($operation eq "top") {
			if ($i == 0){																			# the "wanted" element is put on the top of the array
				$out_array[$i]		= $in_array[$position];
			} elsif ($i <= $position){																# if it's an element before, or at the position, this is swapped the previous element, this is since we're one ahead since we did put the wanted element on top
				$out_array[$i] = $in_array[$i-1];													# [1]    [3]  : element at position 2 is moved to the top
			} else {																				# [2] -> [1]  : the rightmost (out_array) is mapped to the previous
				$out_array[$i] = $in_array[$i];														# [3]    [2]  : leftmost (in_array)
			}
		} elsif ($operation eq "bottom") {
			if ($i == $#in_array){																	# the "wanted" element is put on the bottom of the array
				$out_array[$i]		= $in_array[$position];
			} elsif ($i < $position){
				$out_array[$i] = $in_array[$i];
			} else {
				$out_array[$i] = $in_array[$i+1];
			}
		}
		$i++;
	}

	return @out_array;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		make links out of URLs
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kMakeLinks{
	$text = $_[0];

	my $linkRegExp	= 'http:\/\/[a-zA-Z0-9\/\.\-\_\~]+';
	
	$text =~ s/($linkRegExp)(\)?)(\s?)/<a href="$1" target="_blank">$1<\/a>$2$3/sg;

	return $text;
}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		remove links and only present the URL
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kStripLinks{
	$text			= $_[0];
	
	my $linkRegExp	= 'http:\/\/[a-zA-Z0-9\/\.\-\_]+';
	
	$text =~ s/<a href="$linkRegExp" target="_blank">($linkRegExp)<\/a>/$1/sg;

	return $text;
}